import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class Outputs
{
    /* INSTANCE FIELDS & CONSTRUCTOR */
    private Solution bestSol;
    
    public Outputs(){
        bestSol = null;}

    /* SET METHODS */
    public void setOBSol(Solution obSol){bestSol = obSol;}

    /* GET METHODS */
    public Solution getOBSol(){return bestSol;}

    /* AUXILIARY METHODS */
    public void sendToFile(String outFile)
    {
        try 
        {   PrintWriter out = new PrintWriter(outFile);
            out.println("***************************************************");
            out.println("*                      OUTPUTS                    *");
            out.println("***************************************************");
            out.println("\r\n");
            out.println("--------------------------------------------");
            out.println("\r\n OUR BEST SOLUTION:\r\n");
            out.println("--------------------------------------------");
            out.println(bestSol.toString() + "\r\n");
            out.close();
        } catch (IOException exception) 
        {   System.out.println("Error processing output file: " + exception);
        }
    }
}