import java.util.LinkedList;

/**
 * @author Angel A. Juan - ajuanp(@)gmail.com
 * @version 130112
 */
public class Inputs
{
    /* INSTANCE FIELDS & CONSTRUCTOR */
    private Node[] nodes; // List of all nodes in the problem/sub-problem
    private Edge[][] edges; // List of all edges in the problem/sub-problem
    private float vCap = 0.0F; // Vehicle capacity (homogeneous fleet)
    private int nVeh = 0; // Number of vehicle
    private LinkedList<Edge> savings = null;
    private double[][]  times = null;
    private float[] vrpCenter; // (x-bar, y-bar) is a geometric VRP center
   
    public Inputs(int n)
    {   nodes = new Node[n]; // n nodes, including the depot
        vrpCenter = new float[2];
    }

    /* GET METHODS */
    public Node[] getNodes(){return nodes;}
    public LinkedList<Edge> getSavings(){return savings;}
    public float getVehCap(){return vCap;}
    public int getVehNum(){return nVeh;}
    public float[] getVrpCenter(){return vrpCenter;}
    public double getTime(int i, int j) {return this.times[i][j];}
    public Edge getEdge(int i, int j) {return this.edges[i][j];}

    /* SET METHODS */
    public void setVrpCenter(float[] center){vrpCenter = center;}
    public void setVehCap(float c){vCap = c;}
    public void setList(LinkedList<Edge> sList){savings = sList;}
    public void setTimes(double[][] tList){times = tList;}
    public void setEdgess(Edge[][] edgesList){edges = edgesList;}
    public void setVehNum(int v){nVeh= v;}
	public int getNumNodes(){return nodes.length;}
}