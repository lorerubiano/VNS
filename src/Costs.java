import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Costs {
    private double driverCost, fixedCost, distCost, socialCost;
    private double kpl;
    private double battery;
    private double workingh;
    private double Ld;
  	private double Lt;
  	private double gSocial;
    private double gEnvironmental;
    private double gEconomic;

    /**
     * Creates a Costs object with the arguments specified.
     *
     * @param socialCost Social cost
     * @param driverCost Time cost
     * @param fixedCost Time cost
     * @param distCost Distance cost
     * @param co2Cost CO2 cost
     * @param kpl Fuel consumption rate (km/l)
     * @param driving Range battery life
     * @param allowable working hours
     */
    public Costs(
        double socialCost,
        double driverCost,
        double fixedCost,    
        double distCost,
        double kpl,
        double battery,
        double workingh,
        double Ld,
        double Lt,
        double gSocial, 
        double gEnvironmental, 
        double gEconomic, 
        int penalty,
        double range) {
        this.driverCost = driverCost;
        this.fixedCost = fixedCost;
        this.distCost = distCost;
        this.socialCost = socialCost;
        this.kpl = kpl;
        this.battery = battery;
        this.workingh = workingh;
        this.Ld=Ld;
        this.Lt=Lt;
        this.gSocial=gSocial; 
        this.gEnvironmental=gEnvironmental; 
        this.gEconomic=gEconomic; }

    /**
     *  Creates a Costs object from a file with all attributes
     *  @param parametersFilePath The path to the file
     */
    public Costs(String parametersFilePath) {
    	System.out.println(parametersFilePath);
        try {
            FileReader reader = new FileReader(parametersFilePath);
            Scanner in = new Scanner(reader);
            in.useLocale(Locale.US);
            while (in.hasNextLine()) {
                String s = in.next();
                if (s.charAt(0) == '#') s = null;
                else {
                    socialCost = Double.parseDouble(s);
                    driverCost = in.nextDouble();
                    fixedCost = in.nextDouble();
                    distCost = in.nextDouble();
                    kpl = in.nextDouble();
                    battery = in.nextDouble();
                    workingh = in.nextDouble();
                    Ld = in.nextDouble();
                    Lt = in.nextDouble();
                    gSocial = in.nextDouble();
                    gEnvironmental = in.nextDouble();
                    gEconomic = in.nextDouble();
                }
            }
            in.close();
        } catch (IOException exception) {
            System.out.println("Error processing tests file: " + exception);
        }
    }

    public double getDriverCost() {return driverCost;}
    public double getfixedCost() {return fixedCost;}
    public double getDistCost() {return distCost;}
    public double getSocialCost() {return socialCost;}
    public double getKpl() { return kpl;}
    public double getbattery() { return battery;}
    public double getworkingh() {return workingh;}
    public double getLd() {return Ld;}
  	public double getLt() {return Lt;}
  	public double getgSocial() {return gSocial;}
	public double getgEnvironmental() {return gEnvironmental;}
	public double getgEconomic() {return gEconomic;}
}